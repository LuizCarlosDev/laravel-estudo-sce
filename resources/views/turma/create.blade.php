@extends('layout.app')

@section('conteudo')

    <h3 style="border-bottom:2px solid silver;margin-bottom:10px" class="">Cadastro de Turma</h3>

    <form class="form-horizontal" action="{{ url('turmas/gravar') }}" method="post">

        {!! csrf_field() !!}
        <div class="form-group">
            <div class="col-lg-6">
                <label for="descricao">Descrição</label>
                <input type="text" class="form-control" id="descricao" name="descricao"
                       placeholder="Descrição da turma" autofocus>
            </div>

            <div class="col-lg-3">
                <label for="num">Número</label>
                <input type="tel" class="form-control" id="num" name="num" placeholder="Nº da turma">
            </div>

            <div class="col-lg-2">
                <label for="ano">Ano</label>
                <input type="number" class="form-control" min="2000" max="2060" id="ano" name="ano" placeholder="2016">
            </div>

            <div class="col-lg-1">
                <label for="semestre">Semestre</label>
                <input type="number" class="form-control" min="1" max="2" id="semestre" name="semestre" placeholder="1º">
            </div>
        </div>

        <button type="submit" class="btn btn-primary"><em class="fa fa-save"></em> Gravar</button>
        <a href="{{ url("turmas") }}" class="btn btn-default"><em class="fa fa-undo"></em> Voltar</a>
    </form>

@endsection