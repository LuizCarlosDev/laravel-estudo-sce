@extends('layout.app')

@section('conteudo')

    <a href="{{ url('turmas') }}" class="btn btn-default pull-right">Voltar</a>
    @if(isset($turma) && count($turma) > 0)
        <h3 class="page-header">Turma: {{$turma->descricao}}</h3>
        @foreach($turma->professores as $p)
            <h4>Professor: {{$p->pessoa->nome}}</h4>
        @endforeach
        <table class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>Nome</th>
                <th>Matricula</th>
                <th>Data Nascimento</th>
                <th>Telefone</th>
            </tr>
            </thead>
            <tbody>
            @foreach($turma->alunos as $a)
                <tr>
                    <td>{{$a->pessoa->nome}}</td>
                    <td>{{$a->matricula}}</td>
                    <td>{{$a->pessoa->data_nascimento}}</td>
                    <td>{{$a->pessoa->telefone}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif

@endsection