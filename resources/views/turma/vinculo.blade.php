@extends('layout.app')

@section('header')
    <link href="{{asset('assets/css/select2.min.css')}}" rel="stylesheet">
@endsection
@section('conteudo')

    <h3 style="border-bottom:2px solid silver;margin-bottom:10px" class="">Vincular Alunos</h3>

    <form class="form-horizontal" action="{{ url('turmas/vincular') }}" method="post">

        {!! csrf_field() !!}

        <input type="hidden" name="id_turma" value="{{$turma->id}}">

        <div class="form-group">
            <div class="col-lg-8">
                <label for="turma">Turma</label>
                <input type="text" class="form-control" disabled id="turma" name="turma" value="{{$turma->descricao}}">
            </div>

            <div class="col-lg-4">
                <label for="num">Nº</label>
                <input type="text" class="form-control" disabled id="num" name="num" value="{{$turma->numero}}">
            </div>
        </div>

        <div class="form-group">
            <div class="col-lg-12">
                <label for="alunos">Alunos</label>
                <select id="alunos" name="aluno[]" class="form-control" multiple="multiple" required autofocus>
                    @foreach($alunos as $a)
                        <option value="{{ $a->pessoa_id }}">{{ $a->pessoa->nome }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group">
            <div class="col-lg-12">
                <label for="professores">Professores</label>
                <select id="professores" name="professor" class="form-control" required>
                    @foreach($professores as $p)
                        <option value="{{ $p->pessoa_id }}">{{ $p->pessoa->nome }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <button type="submit" class="btn btn-primary"><em class="fa fa-save"></em> Vincular</button>
        <a href="{{ url("turmas") }}" class="btn btn-default"><em class="fa fa-undo"></em> Voltar</a>
    </form>

@endsection
@section('scripts')
    <script src="{{asset('assets/js/select2.min.js')}}"></script>

    <script>
        $('#alunos').select2({
            placeholder: 'Selecione um ou mais alunos',
            escapeMarkup: function (markup) {
                return markup;
            },
            templateResult: formatRepo
        });

        $('#professores').select2({
            placeholder: 'Selecione um professor',
            escapeMarkup: function (markup) {
                return markup;
            },
            templateResult: formatRepo
        });

        function formatRepo(repo) {
            if (repo.loading) {
                return 'Buscando....';
            }
            var markup = '<div class="clearfix">' +
                    '<div clas="col-sm-10">' +
                    '<div class="clearfix">' +
                    '<div class="col-sm-5">' + repo.id + ' - ' + repo.text + '</div>' +
                    '</div></div></div>';

            return markup;
        }
    </script>
@endsection