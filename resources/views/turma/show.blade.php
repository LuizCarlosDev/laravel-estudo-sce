@extends('layout.app')

@section('conteudo')

    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2>{{$turma->descricao}}</h2>
                </div>
                <form action="{{ url('turmas/remover', $turma->id) }}" method="post">

                    {{ method_field('delete') }}
                    {{ csrf_field() }}

                    <div class="panel-body">
                        <h4>Matricula: </h4>
                        <h4>Data de Nascimento: </h4>
                    </div>
                    <div class="panel-footer">
                        <button type="submit" class="btn btn-danger">Excluir</button>
                        <a href="{{ url('turmas') }}" class="btn btn-primary">Voltar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection