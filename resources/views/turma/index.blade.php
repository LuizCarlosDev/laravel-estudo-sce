@extends('layout.app')

@section('conteudo')

    <h3 class="page-header">Turmas</h3>
    <a href="{{ url('turmas/novo') }}" class="btn btn-primary pull-right">Nova</a>
    @if(isset($turmas) && count($turmas) > 0)
        <table class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>Descriçao</th>
                <th>Nº</th>
                <th>Ano</th>
                <th>Semestre</th>
                <th>Instituicao</th>
                <th>Opções</th>
            </tr>
            </thead>
            <tbody>
            @foreach($turmas as $t)
                <tr>
                    <td>{{$t->descricao}}</td>
                    <td>{{$t->numero}}</td>
                    <td>{{$t->ano}}</td>
                    <td>{{$t->semestre}}</td>
                    <td>{{$t->instituicao->nome}}</td>
                    <td class="text-center">
                        <a href="{{url('turmas/vincular', $t->id)}}" class="btn btn-sm btn-success"><span
                                    class="glyphicon glyphicon-paperclip"></span></a>
                        <a href="{{url('turmas/detalhes', $t->id)}}" class="btn btn-sm btn-primary"><span
                                    class="glyphicon glyphicon-search"></span></a>
                        <a href="{{url('turmas/editar', $t->id)}}" class="btn btn-sm btn-warning"><span
                                    class="glyphicon glyphicon-pencil"></span></a>
                        <a href="{{url('turmas/detalhes', $t->id)}}" class="btn btn-sm btn-danger"><span
                                    class="glyphicon glyphicon-trash"></span></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
            {{ $turmas->links() }}
        </table>
    @else
        <h5>Sem registros!</h5>
    @endif

@endsection