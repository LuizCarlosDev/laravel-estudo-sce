@extends('layout.app')

@section('conteudo')

    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2>{{$aluno->pessoa->nome}}</h2>
                </div>
                <form action="{{ url('alunos/remover', $aluno->pessoa_id) }}" method="post">

                    {{ method_field('delete') }}
                    {{ csrf_field() }}

                    <div class="panel-body">
                        <h4>Matricula: {{$aluno->matricula}}</h4>
                        <h4>Data de Nascimento: {{$aluno->pessoa->data_nascimento}}</h4>
                    </div>
                    <div class="panel-footer">
                        <button type="submit" class="btn btn-danger">Excluir</button>
                        <a href="{{ url('alunos') }}" class="btn btn-primary">Voltar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection