@extends('layout.app')

@section('header')
    <link href="{{asset('assets/css/bootstrap-datepicker.standalone.min.css')}}" rel="stylesheet">
@endsection
@section('conteudo')

    <h3 style="border-bottom:2px solid silver;margin-bottom:10px" class="">Cadastro de Aluno</h3>

    <form class="form-horizontal" action="{{ url('alunos/gravar') }}" method="post">

        {!! csrf_field() !!}
        <div class="form-group">
            <div class="col-lg-8">
                <label for="nome">Nome</label>
                <input type="text" class="form-control" id="nome" name="nome"
                       placeholder="Nome do aluno" autofocus>
            </div>

            <div class="col-lg-4">
                <label for="cpf">CPF</label>
                <input type="tel" class="form-control" id="cpf" name="cpf" placeholder="CPF do aluno">
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-2">
                <label for="data_nascimento">Data Nascimento</label>
                <input type="text" class="form-control" id="data_nascimento" name="data_nascimento"
                       placeholder="Data de nascimento">
            </div>

            <div class="col-lg-4">
                <label for="matricula">Matricula</label>
                <input type="text" class="form-control" id="matricula" name="matricula" placeholder="Nº de matricula">
            </div>

            <div class="col-lg-3">
                <label for="telefone">Telefone</label>
                <input type="tel" class="form-control" id="telefone" name="telefone" placeholder="Telefone">
            </div>

            <div class="col-lg-2">
                <label for="ano">Ano</label>
                <input type="number" class="form-control" min="2000" max="2060" id="ano" name="ano" placeholder="2016">
            </div>

            <div class="col-lg-1">
                <label for="semestre">Semestre</label>
                <input type="number" class="form-control" min="1" max="2" id="semestre" name="semestre" placeholder="1º">
            </div>
        </div>

        <button type="submit" class="btn btn-primary"><em class="fa fa-save"></em> Gravar</button>
        <a href="{{ url("alunos") }}" class="btn btn-default"><em class="fa fa-undo"></em> Voltar</a>
    </form>

@endsection
@section('scripts')
    <script src="{{asset('assets/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap-datepicker.pt-BR.min.js')}}"></script>

    <script>
        $('#data_nascimento').datepicker({
            autoclose: true,
            format: 'dd/mm/yyyy',
            language: 'pt-BR'
        });
    </script>
@endsection