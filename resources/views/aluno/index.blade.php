@extends('layout.app')

@section('header')
    <link href="{{asset('assets/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
@endsection
@section('conteudo')

    <h3 class="page-header">Alunos <a href="{{ url('alunos/novo') }}" class="btn btn-primary pull-right">Novo</a></h3>

    @if(isset($alunos) && count($alunos) > 0)
        <table id="example" class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>Nome</th>
                <th>CPF</th>
                <th>Matricula</th>
                <th>Data Nascimento</th>
                <th>Telefone</th>
                <th>Ano</th>
                <th>Semestre</th>
                <th>Opções</th>
            </tr>
            </thead>
            <tbody>
            @foreach($alunos as $a)
                <tr>
                    <td>{{$a->pessoa->nome}}</td>
                    <td>{{$a->pessoa->cpf}}</td>
                    <td>{{$a->matricula}}</td>
                    <td>{{$a->pessoa->data_nascimento}}</td>
                    <td>{{$a->pessoa->telefone}}</td>
                    <td>{{$a->ano_inicio}}</td>
                    <td>{{$a->semestre_inicio}}</td>
                    <td class="text-center">
                        <a href="{{url('alunos/editar', $a->pessoa_id)}}" class="btn btn-sm btn-warning"><span
                                    class="glyphicon glyphicon-pencil"></span></a>
                        <a href="{{url('alunos/detalhes', $a->pessoa_id)}}" class="btn btn-sm btn-danger"><span
                                    class="glyphicon glyphicon-trash"></span></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <h5>Sem registros!</h5>
    @endif

@endsection
@section('scripts')
    <script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/js/dataTables.bootstrap.min.js')}}"></script>

    <script>
        $(document).ready(function () {
            $('#example').DataTable({
                stateSave: true,
                "language": {
                    "sEmptyTable": "Nenhum registro encontrado",
                    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sInfoThousands": ".",
                    "sLengthMenu": "_MENU_ resultados por página",
                    "sLoadingRecords": "Carregando...",
                    "sProcessing": "Processando...",
                    "sZeroRecords": "Nenhum registro encontrado",
                    "sSearch": "Pesquisar",
                    "oPaginate": {
                        "sNext": "Próximo",
                        "sPrevious": "Anterior",
                        "sFirst": "Primeiro",
                        "sLast": "Último"
                    },
                    "oAria": {
                        "sSortAscending": ": Ordenar colunas de forma ascendente",
                        "sSortDescending": ": Ordenar colunas de forma descendente"
                    }
                },
                "columnDefs": [
                    {
                        "targets": [7],
                        "visible": true,
                        "searchable": false
                    }
                ]
            });
        });
    </script>

@endsection