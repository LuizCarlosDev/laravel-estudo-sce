<?php

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => ['web']], function(){

    Route::group(['prefix' => 'alunos'], function(){

        Route::get('/',                ['as'=>'aluno.index',  'uses'=>'AlunoController@index']);
        Route::get('/novo',            ['as'=>'aluno.create', 'uses'=>'AlunoController@create']);
        Route::post('/gravar',         ['as'=>'aluno.store',  'uses'=>'AlunoController@store']);
        Route::get('/detalhes/{id}',   ['as'=>'aluno.show',   'uses'=>'AlunoController@show']);
        Route::get('/editar/{id}',     ['as'=>'aluno.edit',   'uses'=>'AlunoController@edit']);
        Route::put('/atualizar/{id}',  ['as'=>'aluno.update', 'uses'=>'AlunoController@update']);
        Route::delete('/remover/{id}', ['as'=>'aluno.delete', 'uses'=>'AlunoController@destroy']);

    });

    Route::group(['prefix' => 'turmas'], function(){

        Route::get('/',                ['as'=>'turma.index',    'uses'=>'TurmaController@index']);
        Route::get('/novo',            ['as'=>'turma.create',   'uses'=>'TurmaController@create']);
        Route::post('/gravar',         ['as'=>'turma.store',    'uses'=>'TurmaController@store']);
        Route::get('/detalhes/{id}',   ['as'=>'turma.show',     'uses'=>'TurmaController@show']);
        Route::get('/editar/{id}',     ['as'=>'turma.edit',     'uses'=>'TurmaController@edit']);
        Route::put('/atualizar/{id}',  ['as'=>'turma.update',   'uses'=>'TurmaController@update']);
        Route::delete('/remover/{id}', ['as'=>'turma.delete',   'uses'=>'TurmaController@destroy']);
        Route::get('/vincular/{id}',   ['as'=>'turma.vinculo',  'uses'=>'TurmaController@vinculo']);
        Route::post('/vincular',       ['as'=>'turma.vincular', 'uses'=>'TurmaController@vincular']);

    });

});