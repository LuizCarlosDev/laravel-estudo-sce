<?php

namespace App\Http\Controllers;

use App\Models\Aluno;
use App\Repositories\Contracts\IAluno;
use Illuminate\Http\Request;

use App\Http\Requests;

class AlunoController extends Controller
{
    private $repository;

    public function __construct(IAluno $alunoRepository)
    {
        $this->repository = $alunoRepository;
    }

    public function index()
    {
        $alunos = $this->repository->index();
        return view('aluno.index', ['alunos' => $alunos]);
    }

    public function create()
    {
        return view('aluno.create');
    }

    public function store(Request $request)
    {
        $gravou = $this->repository->store($request->all());

        if($gravou){
            return redirect()->route('aluno.index');
        }

        return redirect()->back();
    }

    public function show($id)
    {
        $aluno = $this->repository->show($id);
        return view('aluno.show', ['aluno' => $aluno]);
    }

    public function edit($id)
    {
        $aluno = $this->repository->show($id);
        return view('aluno.edit', ['aluno' => $aluno]);
    }

    public function update(Request $request, $id)
    {
        $gravou = $this->repository->update($request->all(), $id);

        if($gravou){
            return redirect()->route('aluno.index');
        }

        return redirect()->back();
    }

    public function destroy($id)
    {
        $removeu = $this->repository->destroy($id);

        if($removeu){
            return redirect()->route('aluno.index');
        }

        return redirect()->back();
    }
}
