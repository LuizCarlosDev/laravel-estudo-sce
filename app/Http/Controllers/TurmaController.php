<?php

namespace App\Http\Controllers;

use App\Models\Aluno;
use App\Models\Professor;
use App\Models\Turma;
use Illuminate\Http\Request;

use App\Http\Requests;

class TurmaController extends Controller
{
    private $turma;

    public function __construct(Turma $turma)
    {
        $this->turma = $turma;
    }

    public function index()
    {
        $turmas = $this->turma->paginate(5);

        return view('turma.index', ['turmas' => $turmas]);
    }

    public function create()
    {
        return view('turma.create');
    }

    public function store(Request $request)
    {

    }

    public function show($id)
    {
        $turma = $this->turma->find($id);
        return view('turma.details', ['turma' => $turma]);
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        $excluiu = $this->turma->destroy($id);

        if($excluiu){
            return redirect()->route('turma.index');
        }
        return redirect()->back();
    }

    public function vinculo($id)
    {
        $turma = $this->turma->find($id);
        $alunos = Aluno::all();
        $professores = Professor::all();
        return view('turma.vinculo', ['turma' => $turma, 'alunos' => $alunos, 'professores' => $professores]);
    }

    public function vincular(Request $request)
    {
        dd($request->all());
        $alunos_id = $request->input('aluno');
        $professor = $request->input('professor');
        $turma_id = $request->input('id_turma');

        $turma = $this->turma->find($turma_id);

        $gravouAlunos = $turma->alunos()->sync($alunos_id);
        $gravouProfessor = $turma->professores()->sync([$professor]);

        if(!empty($gravouAlunos['attached']) && !empty($gravouProfessor['attached'])){
            return redirect()->route('turma.index');
        }
        return redirect()->back();
    }
}
