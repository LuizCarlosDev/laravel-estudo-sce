<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Aluno extends Model
{
    protected $table = 'alunos';
    public $primaryKey = 'pessoa_id';
    public $timestamps = false;
    protected $fillable = ['matricula', 'ano_inicio', 'semestre_inicio'];

    public function pessoa()
    {
        return $this->belongsTo(Pessoa::class);
    }

    public function turmas()
    {
        return $this->belongsToMany(Turma::class);
    }
}
