<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Instituicao extends Model
{
    protected $table = 'instituicao';
    public $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = ['nome', 'cnpj'];

    public function turmas(){
        return $this->belongsToMany(Turma::class);
    }
}
