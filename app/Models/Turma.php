<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Turma extends Model
{
    protected $table = 'turmas';
    public $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = ['numero', 'descricao', 'ano', 'semestre'];

    public function instituicao()
    {
        return $this->belongsTo(Instituicao::class);
    }

    public function professores()
    {
        return $this->belongsToMany(Professor::class);
    }

    public function alunos()
    {
        return $this->belongsToMany(Aluno::class);
    }
}
