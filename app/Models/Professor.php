<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Professor extends Model
{
    protected $table = 'professores';
    public $primaryKey = 'pessoa_id';
    public $timestamps = false;
    protected $fillable = ['registro', 'titulacao_maxima'];

    public function pessoa()
    {
        return $this->belongsTo(Pessoa::class);
    }

    public function turmas()
    {
        return $this->belongsToMany(Turma::class);
    }
}
