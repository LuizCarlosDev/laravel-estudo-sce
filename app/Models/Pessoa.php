<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pessoa extends Model
{
    protected $table = 'pessoas';
    public $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = ['nome', 'cpf', 'data_nascimento', 'telefone'];

    public function aluno(){
        return $this->hasOne(Aluno::class);
    }

    public function professor(){
        return $this->hasOne(Professor::class);
    }
}
