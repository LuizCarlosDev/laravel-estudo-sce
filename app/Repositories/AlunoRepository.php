<?php

namespace App\Repositories;

use App\Models\Aluno;
use App\Models\Pessoa as Pessoa;

use App\Repositories\Contracts\IAluno;
use Carbon\Carbon;

class AlunoRepository implements IAluno
{
    private $aluno;

    public function __construct(Aluno $aluno)
    {
        $this->aluno = $aluno;
    }

    public function index()
    {
        return $this->aluno->all();
    }

    public function show($id)
    {
        return $this->aluno->find($id);
    }

    public function store($data)
    {
        $dia = substr($data['data_nascimento'], 0, 2);
        $mes = substr($data['data_nascimento'], 3, 2);
        $ano = substr($data['data_nascimento'], 6, 4);
        $data_nascimento = Carbon::create($ano, $mes, $dia, 0, 0, 0);

        //dd($data_nascimento);
        $pessoa = Pessoa::create([
            'nome' => $data['nome'],
            'cpf' => $data['cpf'],
            'data_nascimento' => $data_nascimento,
            'telefone' => $data['telefone'],
        ]);

        $aluno = new Aluno([
            'matricula' => $data['matricula'],
            'ano_inicio' => $data['ano'],
            'semestre_inicio' => $data['semestre']
        ]);

        if ($pessoa && $aluno) {
            $pessoa->aluno()->save($aluno);
            return true;
        }
        return false;
    }

    public function update($data, $id)
    {
        $dia = substr($data['data_nascimento'], 0, 2);
        $mes = substr($data['data_nascimento'], 3, 2);
        $ano = substr($data['data_nascimento'], 6, 4);
        $data_nascimento = Carbon::create($ano, $mes, $dia, 0, 0, 0);

        $aluno = $this->show($id);

        $aluno->matricula = $data['matricula'];
        $aluno->ano_inicio = $data['ano'];
        $aluno->semestre_inicio = $data['semestre'];
        $aluno->pessoa->nome = $data['nome'];
        $aluno->pessoa->cpf = $data['cpf'];
        $aluno->pessoa->telefone = $data['telefone'];
        $aluno->pessoa->data_nascimento = $data_nascimento;

        if($aluno->pessoa->save() && $aluno->save()){
            return true;
        }

        return false;
    }

    public function destroy($id)
    {
        return Pessoa::destroy($id);
    }
}