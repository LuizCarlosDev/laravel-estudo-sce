<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alunos', function(Blueprint $table)
        {
            $table->foreign('pessoa_id', 'aluno_1_fk')->references('id')->on('pessoas')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::table('professores', function(Blueprint $table)
        {
            $table->foreign('pessoa_id', 'professor_1_fk')->references('id')->on('pessoas')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::table('turmas', function(Blueprint $table)
        {
            $table->foreign('instituicao_id', 'instituicao_1_fk')->references('id')->on('instituicao')->onUpdate('cascade')->onDelete('restrict');
        });

        Schema::table('professor_turma', function(Blueprint $table)
        {
            $table->foreign('professor_id', 'professor_turma_1_fk')->references('pessoa_id')->on('professores')->onUpdate('cascade')->onDelete('restrict');
            $table->foreign('turma_id', 'professor_turma_2_fk')->references('id')->on('turmas')->onUpdate('cascade')->onDelete('restrict');
        });

        Schema::table('aluno_turma', function(Blueprint $table)
        {
            $table->foreign('aluno_id', 'aluno_turma_1_fk')->references('pessoa_id')->on('alunos')->onUpdate('cascade')->onDelete('restrict');
            $table->foreign('turma_id', 'aluno_turma_2_fk')->references('id')->on('turmas')->onUpdate('cascade')->onDelete('restrict');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alunos', function(Blueprint $table)
        {
            $table->dropForeign('aluno_1_fk');
        });

        Schema::table('professores', function(Blueprint $table)
        {
            $table->dropForeign('professor_1_fk');
        });

        Schema::table('turmas', function(Blueprint $table)
        {
            $table->dropForeign('instituicao_1_fk');
        });

        Schema::table('professor_turma', function(Blueprint $table)
        {
            $table->dropForeign('professor_turma_1_fk');
            $table->dropForeign('professor_turma_2_fk');
        });

        Schema::table('aluno_turma', function(Blueprint $table)
        {
            $table->dropForeign('aluno_turma_1_fk');
            $table->dropForeign('aluno_turma_2_fk');
        });
    }
}
