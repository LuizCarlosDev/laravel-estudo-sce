<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfessorTurmaTable extends Migration
{
    /**
     * Run the migrations.
     *php artisan make:migration create_professor_turma_table --create="professor_turma"
     * @return void
     */
    public function up()
    {

        Schema::create('professor_turma', function (Blueprint $table) {
            $table->unsignedBigInteger('professor_id');
            $table->unsignedBigInteger('turma_id');
            $table->primary(array('professor_id','turma_id'));
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('professor_turma');
    }
}
