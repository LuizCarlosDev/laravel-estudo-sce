<?php

use Illuminate\Database\Seeder;

class AlunoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Pessoa::class, 20)->create()->each(function($u) {
            $u->aluno()->save(factory(App\Models\Aluno::class)->make());
        });
    }
}
