<?php

use Illuminate\Database\Seeder;

class ProfessorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Pessoa::class, 10)->create()->each(function($u) {
            $u->aluno()->save(factory(App\Models\Professor::class)->make());
        });
    }
}
