<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* Use php artisan db:seed */
        $this->call(AlunoSeeder::class);
        $this->call(ProfessorSeeder::class);
        $this->call(InstituicaoSeeder::class);
        $this->call(TurmaSeeder::class);
    }
}
