<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Models\Pessoa::class, function (Faker\Generator $faker) {
    return [
        'nome' => $faker->name,
        'telefone' => $faker->randomNumber(8),
        'data_nascimento' => $faker->dateTime,
        'cpf' => $faker->randomNumber(9). $faker->randomNumber(3),
    ];
});

$factory->define(App\Models\Aluno::class, function (Faker\Generator $faker) {
    return [
        'matricula' => $faker->randomNumber(6),
        'ano_inicio' => $faker->numberBetween(2000, 2016),
        'semestre_inicio' => $faker->numberBetween(1, 2),
    ];
});

$factory->define(App\Models\Professor::class, function (Faker\Generator $faker) {
    return [
        'registro' => $faker->randomNumber(6),
        'titulacao_maxima' => $faker->numberBetween(1, 9),
    ];
});

$factory->define(App\Models\Instituicao::class, function (Faker\Generator $faker) {
    return [
        'nome' => $faker->company,
        'cnpj' => $faker->randomNumber(9). $faker->randomNumber(5),
    ];
});

$factory->define(App\Models\Turma::class, function (Faker\Generator $faker) {
    return [
        'descricao' => $faker->company,
        'numero' => $faker->numberBetween(1, 9),
        'ano' => $faker->numberBetween(2000, 2016),
        'semestre' => $faker->numberBetween(1, 2),
    ];
});